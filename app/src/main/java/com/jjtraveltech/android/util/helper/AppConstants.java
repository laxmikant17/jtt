package com.jjtraveltech.android.util.helper;

/**
 * Created by Laxmikant Mahamuni on 20-04-2018.
 */

public interface AppConstants {

    String ANDROID_CHANNEL_ID = "INTL-ANDROID:888888";

    String BASE_URL = "https://5e99a9b1bc561b0016af3540.mockapi.io/";

    String SHARED_PREFS_NAME = "APP_PREFERENCE";
    String PREFS_LANGUAGE_CODE = "prefs_language_code";
    String PREFS_COUNTRY_CODE = "prefs_country_code";
    String PREFS_RESTAURANT_COUNTRY = "prefs_restaurant_country";
    String PREFS_NEAR_ME_CLICKED = "prefs_near_me_clicked";
    String HIDE_KM = "hideKM";

    enum ENUM_COUNTRIES {DE, DEU}

    String TAG_DEV_LOG = " ";

    class RealmFields {
        public static final String FIELD_NAME = "name";
        public static final String FIELD_IS_BOOKABLE = "isBookable";
        public static final String FIELD_TYPE_NAME = "typeName";
        public static final String RESTAURANT_ID = "restaurantId";
        public static final String APP_SETTING_ID = "id";

    }

    class PreferenceTags {
        public static final String PREFS_OFFLINE_SYNC_REQUIRED = "prefs_is_sync_required";
        public static final String PREFS_REALM_NAME = "prefs_database_name";
        public static final String PREFS_REALM_DIRECTORY_PATH = "prefs_directory_path";
        public static final String PREFS_DEFAULT_CITY_NAME = "prefs_default_city_name";
        public static final String PREFS_LAST_SEQUENCE = "prefs_last_sequence";
        public static final int SCHEMA_VERSION = 2;
        public static final String BAT_VERSION_CODE = "prefs_version_code";
    }

    class BundleExtras {
        public static final String EXTRA_MRAS_FULL_DATE = "extra_mras_full_date";
    }
}