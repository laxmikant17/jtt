package com.jjtraveltech.android.uimodules.articleList.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.jjtraveltech.android.R;
import com.jjtraveltech.android.base.BaseFragment;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;
import com.jjtraveltech.android.uimodules.articleList.adapters.ArticleAdapter;
import com.jjtraveltech.android.uimodules.articleList.presenter.BaseListPresenter;
import com.jjtraveltech.android.util.helper.AppConstants;
import com.jjtraveltech.android.util.helper.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;



public abstract class BaseListFragment extends BaseFragment implements AppConstants, ArticleListView {

    private static final int REQUEST_PLAY_SERVICE = 1221;


    BaseListPresenter presenter;

    @BindView(R.id.recycler_restaurants)
    protected RecyclerView recyclerRestaurants;
    protected ArrayList<ArticlesResponse> articlesResponses;

    protected LinearLayoutManager linearLayoutManager;
    @BindView(R.id.error_view)
    protected LinearLayout errorView;
    @BindView(R.id.error_image)
    ImageView errorImage;
    @BindView(R.id.error_text_header)
    TextView errorTextHeader;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.error_text_navigation_link)
    TextView navigationLink;

    @BindView(R.id.progressLayout)
    LinearLayout progLinearLayout;
    public boolean tabIsLoading = false;
    public static final String TAG_DATA = "data";

    private static final int DIALOG_FRAGMENT = 1;
    private View returnView;
    private Calendar selectedDateFromPicker = Calendar.getInstance();
    private ArticleAdapter articleAdapter;

    public abstract BaseListPresenter getPresenter();

    public abstract Object getListAdapter();

    public abstract void resetListData();

    public abstract void resetListAdapter();

    public abstract void checkDataAndUpdateList();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        returnView = inflater.inflate(R.layout.fragment_article_list, container, false);
        ButterKnife.bind(this, returnView);

        articlesResponses = new ArrayList<>();

        presenter = getPresenter();
        presenter.onViewCreated();
        return returnView;
    }

      public void showSpinner() {
        tabIsLoading = true;
        progLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSpinner() {
        tabIsLoading = false;
        progLinearLayout.setVisibility(View.GONE);
    }

    boolean errorScreenShown = false;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case REQ_CODE_SPEECH_INPUT:
                    if (null != data) {
                        ArrayList<String> result = data
                                .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    }
                    break;

                case DIALOG_FRAGMENT:
                    if (data.getExtras() != null) {
                        if (data.hasExtra(BundleExtras.EXTRA_MRAS_FULL_DATE))
                            selectedDateFromPicker.setTime((Date) data.getSerializableExtra(BundleExtras.EXTRA_MRAS_FULL_DATE));
//                        setMRASData(data);
                        checkDataAndUpdateList();
                    }
                    break;

                case REQUEST_PLAY_SERVICE:
                    presenter.onPlayServiceUpdated();

                    resetListData();


                    articleAdapter = (ArticleAdapter) getListAdapter();
                    if (articleAdapter != null)
                        articleAdapter.setData(new ArrayList<>());

                    recyclerRestaurants.setVisibility(View.GONE);
                    presenter.onViewCreated();
            }
        } else {
            if (data != null && data.hasExtra("CANCEL_CLICKED") && data.getBooleanExtra("CANCEL_CLICKED", false)) {
                recyclerRestaurants.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public void onDestroy() {
        presenter.onViewDetach();
        super.onDestroy();
        Timber.d("List onDestroy() method");
    }

    @Override
    public void onPause() {
        super.onPause();
        Timber.d("List onPause() method");
    }

    public boolean ifGooglePlaysServiceAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        return status == ConnectionResult.SUCCESS;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_article_list;
    }

    @Override
    protected void onRetryClickedForInternetCheck() {
    }


    @Override
    protected void hideContent() {
        hideErrorView();
    }

    @Override
    protected void showContent() {
        recyclerRestaurants.setVisibility(View.VISIBLE);
    }

    @Inject
    public SharedPreferences sharedPreferences;

    @Override
    public void hideErrorView() {
        if (recyclerRestaurants != null && errorView != null && errorText != null) {
            recyclerRestaurants.setVisibility(View.GONE);
            errorView.setVisibility(View.GONE);
            errorText.setVisibility(View.VISIBLE);
        }
    }
}
