package com.jjtraveltech.android.uimodules.articleList.view

import com.jjtraveltech.android.base.BaseView
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse
import java.util.*

interface ArticleListView : BaseView {

    fun showSpinner()

    fun hideSpinner()

    fun showData(articlesResponseArrayList: ArrayList<ArticlesResponse>)

    fun updateData(articlesResponseArrayList: ArrayList<ArticlesResponse>)

    fun hideErrorView()

}