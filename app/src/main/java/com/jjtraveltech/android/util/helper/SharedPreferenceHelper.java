package com.jjtraveltech.android.util.helper;

import android.content.Context;
import android.content.SharedPreferences;
import static com.jjtraveltech.android.util.helper.AppConstants.SHARED_PREFS_NAME;

public class SharedPreferenceHelper {

    private static SharedPreferenceHelper instance;

    private SharedPreferenceHelper() {}

    public static SharedPreferenceHelper getInstance()
    {
        if(instance==null)
        {
            instance = new SharedPreferenceHelper();
        }

        return instance;
    }

    public SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences.Editor getSharedPreferencesEditor(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE).edit();
    }

    public String getStringValue(Context context, String key) {
        return getSharedPreferences(context).getString(key, null);
    }

    public void setStringValue(Context context, String key, String value) {
        getSharedPreferencesEditor(context).putString(key, value).commit();
    }

    public void saveChannelId(Context context, String key, String value) {
        getSharedPreferencesEditor(context).putString(key, value).commit();
    }

    public String getChannelId(Context context, String key) {
        return getSharedPreferences(context).getString(key, AppConstants.ANDROID_CHANNEL_ID);
    }

    // whether to show address line 2
    // enum is defined for countries not showing address line 2
    // and also swap postal code and city

    public boolean hideAddress2AndSwapPostCity(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        String countryCode = sharedPreferences.getString(AppConstants.PREFS_RESTAURANT_COUNTRY, "GBR");

        for (AppConstants.ENUM_COUNTRIES enumCountries : AppConstants.ENUM_COUNTRIES.values()) {
            if (enumCountries.name().equalsIgnoreCase(countryCode)) {
                return true;
            }
        }

        return false;
    }

}
