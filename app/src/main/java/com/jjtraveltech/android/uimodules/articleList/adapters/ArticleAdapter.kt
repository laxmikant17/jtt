package com.jjtraveltech.android.uimodules.articleList.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse
import java.util.*

class ArticleAdapter(private val activity: Activity) : BaseListAdapter(activity) {
    private var viewHolder: RestaurantViewHolder? = null
    private var overviewArrayList: ArrayList<ArticlesResponse>? = null

    override fun getRestaurantViewHolder(): RestaurantViewHolder? {
        return viewHolder
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val view = LayoutInflater.from(activity).inflate(com.jjtraveltech.android.R.layout.adapter_article_item, parent, false)
        viewHolder = RestaurantViewHolder(view)
        return viewHolder as RestaurantViewHolder
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        this.overviewArrayList = listData
        if (overviewArrayList != null && overviewArrayList!!.size != 0) {
            val articleOverview = overviewArrayList!![position]
            bindDataToViews(articleOverview, holder, position)
        }
    }

    private fun bindDataToViews(articleOverview: ArticlesResponse?, holder: RestaurantViewHolder, position: Int) {
        if (articleOverview != null && articleOverview.user != null
                && articleOverview.user.size > 0) {

            val name = articleOverview.user[0].name
            holder.txtRestaurantName.text = name

            if (articleOverview.media != null
                    && articleOverview.media.size > 0) {
                val imageUrl = articleOverview.media[0].image

                loadImage(imageUrl, holder.imgRestaurant, holder.imgRestaurantError, holder.textViewImageError)
            }
        }
    }

}
