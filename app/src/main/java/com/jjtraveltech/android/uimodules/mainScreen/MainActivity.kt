package com.jjtraveltech.android.uimodules.mainScreen

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.jjtraveltech.android.uimodules.articleList.view.ArticlesListFragment
import com.jjtraveltech.android.util.helper.AppConstants


class MainActivity : AppCompatActivity(), AppConstants {

    var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.jjtraveltech.android.R.layout.activity_main)

        val fm = supportFragmentManager
        fragment = fm.findFragmentByTag("myFragmentTag")

        if (fragment == null) {
            val ft = fm.beginTransaction()
            fragment = ArticlesListFragment()
            ft.add(android.R.id.content, fragment, "myFragmentTag")
            ft.commit()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {}
}
