package com.jjtraveltech.android.uimodules.mainScreen.interfaces

import com.jjtraveltech.android.dataconverters.database.RealmCreator
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result
import java.util.*


interface LocalDataRepository {

//    fun addOrUpdateArticleList(articleList: ArrayList<class_result>, realmCreator: RealmCreator)
    fun addOrUpdateArticleList(articleList: ArrayList<class_result>)

}
