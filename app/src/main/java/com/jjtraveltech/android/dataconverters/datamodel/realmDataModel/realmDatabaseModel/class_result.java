package com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class class_result extends RealmObject {

    @PrimaryKey
    private String id;

    private String createdAt;

    private String comments;

    private String content;

    private String likes;

    private String imageURL;

    public class_result() {
    }

    public class_result(String id, String createdAt, String comments, String content, String likes, String imageURL) {
        this.id = id;
        this.createdAt = createdAt;
        this.comments = comments;
        this.content = content;
        this.likes = likes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "ClassPojo [createdAt = " + createdAt + ", comments = " + comments + ", id = " + id + ", media = " + ", user = " + ", content = " + content + ", likes = " + likes + "]";
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}