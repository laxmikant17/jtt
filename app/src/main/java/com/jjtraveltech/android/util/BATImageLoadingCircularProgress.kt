package com.jjtraveltech.android.util

import android.content.Context
import android.graphics.PorterDuff
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable

class BATImageLoadingCircularProgress
/**
 * @param context application context
 */
(context: Context) : CircularProgressDrawable(context) {
    init {
        customize(context)
        start()

    }

    private fun customize(context: Context) {
        strokeWidth = 12f
        centerRadius = 50f
        setColorFilter(ContextCompat.getColor(context, com.jjtraveltech.android.R.color.colorAccent), PorterDuff.Mode.SRC)
    }
}
