package com.jjtraveltech.android.dataconverters

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.adapter.ArticleOverview
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result

import java.util.ArrayList

interface ArticleMapper {
    fun convertToRestaurantAdapter(restaurants: ArrayList<class_result>, shouldShowDistance: Boolean): ArrayList<ArticleOverview>
}
