package com.jjtraveltech.android.uimodules.articleList.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjtraveltech.android.JTTApplication;
import com.jjtraveltech.android.R;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;
import com.jjtraveltech.android.util.BATImageLoadingCircularProgress;
import com.jjtraveltech.android.util.GlideApp;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseListAdapter extends RecyclerView.Adapter<BaseListAdapter.RestaurantViewHolder> {

    private Activity activity;
    private ArrayList<ArticlesResponse> overviewArrayList;

    protected abstract RestaurantViewHolder getRestaurantViewHolder();

    public BaseListAdapter(Activity activity) {
        this.activity = activity;
        overviewArrayList = new ArrayList<>();
        injectDependency();
    }

    private void injectDependency() {
        JTTApplication.getInstance().getRestaurantListComponent().inject(this);
    }

    public void setData(ArrayList<ArticlesResponse> overviewArrayList) {
        this.overviewArrayList = overviewArrayList;
    }

    public ArrayList<ArticlesResponse> getListData() {
        return overviewArrayList;
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return getRestaurantViewHolder();
    }


    @Override
    public int getItemCount() {
        return overviewArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(overviewArrayList.get(position).getId());
    }


    public void loadImage(String imageID, ImageView imageView, ImageView imageViewError, TextView textViewError) {

        BATImageLoadingCircularProgress batImageLoadingCircularProgress = new BATImageLoadingCircularProgress(activity);
        textViewError.setVisibility(View.INVISIBLE);
        imageViewError.setVisibility(View.INVISIBLE);
        GlideApp
                .with(activity)
                .load(imageID)
                .placeholder(batImageLoadingCircularProgress)
                .normalImage()
                .into(imageView);
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.root_restaurant_item)
        ConstraintLayout constraintLayout;
        @BindView(R.id.card)
        CardView card;

        @BindView(R.id.img_restaurant)
        ImageView imgRestaurant;
        @BindView(R.id.text_view_image_error)
        TextView textViewImageError;
        @BindView(R.id.img_restaurant_error)
        ImageView imgRestaurantError;

        @BindView(R.id.txt_restaurant_name)
        AppCompatTextView txtRestaurantName;
        @BindView(R.id.txt_city_name)
        TextView txtCityName;
        @BindView(R.id.txt_distance)
        TextView txtDistance;
        @BindView(R.id.root_distance)
        LinearLayout rootDistance;

        @BindView(R.id.text_view_is_star_deal)
        public TextView txtIsStarDeal;
        @BindView(R.id.text_view_offer_text)
        public TextView txtStarDealText;


        @BindView(R.id.btnShowAvailability)
        Button btnShowAvailability;

        @BindView(R.id.tvNoRestaurantAvailableMessage)
        TextView tvNoRestaurantAvailableMessage;

        RecyclerView recyclerViewHorizontalList;


        public RestaurantViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            card.setOnClickListener(this);
            recyclerViewHorizontalList = itemView.findViewById(R.id.idRecyclerViewHorizontalList);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position == -1)
                return;
        }
    }
}
