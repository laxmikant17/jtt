package com.jjtraveltech.android.uimodules.articleList.extractDatabase;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository;
import com.jjtraveltech.android.util.helper.AppConstants;
import com.jjtraveltech.android.util.helper.Environment;
import com.jjtraveltech.android.util.helper.SharedPreferenceHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;


public class DBSetupAsyncTask extends AsyncTask<String, String, String> {
    protected Context context;
    private SharedPreferences sharedPreferences;
    LocalRepository localRepository;

    public DBSetupAsyncTask(Context context, SharedPreferences sharedPreferences, LocalRepository localRepository) {
        this.context = context;
        this.sharedPreferences = sharedPreferences;
        this.localRepository = localRepository;
    }

    @Override
    protected String doInBackground(String... voids) {
        copyDataBaseToDir();
        return null;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
//            restaurantSyncRequest.initRequest();
    }

    private void copyDataBaseToDir() {
        if (context == null)
            return;
        if (sharedPreferences == null)
            return;
        boolean isParentDirectoryExist = checkIfParentDirectoryExist();
        Timber.d("Is parent directory exist? => %s", isParentDirectoryExist);
        String filename = Environment.INSTANCE.getFileNameByEnvironment();
        AssetManager assetManager = context.getAssets();
        try {
            InputStream in = assetManager.open(filename);
            //absolute path to the directory on the filesystem, filename
            File outFile = new File(context.getFilesDir(), filename);
            OutputStream out = new FileOutputStream(outFile);
            Timber.d("Internal file path is %s and file name is %s", outFile.getAbsolutePath(), filename);
            //data/user/0/com.jjtraveltech.android.debug/files/jtt.realm
            Timber.d("Copying contents of assets in internal storage");
            copyFileContentsToDestination(in, out);
            setupRealmForFirstTime(context, outFile);
            in.close();
            out.flush();
            out.close();
        } catch (Exception e) {
            Timber.e(e, "Failed to copy asset file: %s", filename);
        }
    }

    private boolean checkIfParentDirectoryExist() {
        String parentPath = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, "");
        File file = new File(parentPath);
        return file.exists();
    }


    /**
     * copies content of input stream (db - asset file) to internal db file
     *
     * @param in  input stream instance
     * @param out output stream instance
     * @throws IOException ioException
     */
    private static void copyFileContentsToDestination(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    /**
     * Sets up realm database and sharedpreference
     * Change migration version if table is modified or new table is created
     */
    private void setupRealmForFirstTime(Context context, File filepath) {

        Timber.d("Database file is now being created");
        Realm.init(context);
        final RealmConfiguration configuration = new RealmConfiguration.Builder().
                directory(filepath.getParentFile()).
                name(filepath.getName()).
                schemaVersion(AppConstants.PreferenceTags.SCHEMA_VERSION).
                build();
        Realm.setDefaultConfiguration(configuration);

        SharedPreferences.Editor editor = SharedPreferenceHelper.getInstance().getSharedPreferencesEditor(context);
        editor.putString(AppConstants.PreferenceTags.PREFS_REALM_NAME, filepath.getName()).apply();
        editor.putString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, filepath.getParentFile().getAbsolutePath()).apply();

//        Log.d("DB_Log", "Insertion started");
//        ArrayList<class_result> databaseCompatible = new ArrayList<>();
////        for (ArticlesResponse articlesResponses : response) {
//
//        class_result classresult = new class_result();
//        classresult.setId("articlesResponses.getId()");
//        classresult.setCreatedAt("articlesResponses.getCreatedAt()");
//        classresult.setComments("articlesResponses.getComments()");
//        classresult.setContent("articlesResponses.getContent()");
//        classresult.setLikes("articlesResponses.getLikes()");
////            if (articlesResponses.getMedia() != null
////                    && articlesResponses.getMedia().size() > 0
////                    && articlesResponses.getMedia().get(0).getImage() != null)
////                classresult.setImageURL(articlesResponses.getMedia().get(0).getImage());
//
//        databaseCompatible.add(classresult);
////        }
//        localRepository.addOrUpdateArticleList(databaseCompatible);
//        Log.d("DB_Log", "Insertion completed!!!");
//            realm.close();
    }
}
