package com.jjtraveltech.android.uimodules.articleList.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.jjtraveltech.android.JTTApplication;
import com.jjtraveltech.android.dagger.module.ArticleModule;
import com.jjtraveltech.android.dataconverters.database.RealmCreator;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;
import com.jjtraveltech.android.uimodules.articleList.adapters.ArticleAdapter;
import com.jjtraveltech.android.uimodules.articleList.presenter.ArticleListPresenter;
import com.jjtraveltech.android.uimodules.articleList.presenter.BaseListPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

public class ArticlesListFragment extends BaseListFragment  {

    @Inject
    ArticleListPresenter restaurantListPresenter;
    ArticleAdapter articleAdapter;
    Context activity;

    @Inject
    public RealmCreator realmCreator;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getContext();
        injectDependency();
    }


    private void injectDependency() {
        ArticleModule articleModule = null;
        articleModule = new ArticleModule(this);
        JTTApplication.getInstance().getRestaurantComponent(articleModule).inject(this);
    }

    public static Fragment newInstance() {
        return new ArticlesListFragment();
    }

    @Override
    public BaseListPresenter getPresenter() {
        return restaurantListPresenter;
    }

    @Override
    public Object getListAdapter() {
        return articleAdapter;
    }


    @Override
    public void showData(ArrayList<ArticlesResponse> articlesResponseArrayList) {
        if (articlesResponseArrayList != null) {
            this.articlesResponses = articlesResponseArrayList;
            recyclerRestaurants.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.INVISIBLE);
            articleAdapter = new ArticleAdapter(getActivity());
            linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerRestaurants.setAdapter(articleAdapter);
            recyclerRestaurants.setLayoutManager(linearLayoutManager);
            articleAdapter.setData(articlesResponses);

        } else {
            recyclerRestaurants.setVisibility(View.INVISIBLE);
        }
    }

    public void resetListData() {
        showSpinner();
        recyclerRestaurants.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updateData(ArrayList<ArticlesResponse> articlesResponseArrayList) {
        this.articlesResponses.addAll((articlesResponses.size()), articlesResponseArrayList);
        articleAdapter.setData(articlesResponses);
    }


    @Override
    public void resetListAdapter() {
        if (articleAdapter != null) {
            articleAdapter.setData(new ArrayList<>());
            articleAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void checkDataAndUpdateList() {
        if (presenter.articlesResponses != null && presenter.articlesResponses.size() > 0) {
            resetListAdapter();
        }
    }
}
