package com.jjtraveltech.android;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.jjtraveltech.android.uimodules.articleList.extractDatabase.DBSetupAsyncTask;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository;
import com.jjtraveltech.android.util.helper.SharedPreferenceHelper;


/**
 * Created by Laxmikant Mahamuni on 22-03-2018.
 */
public class JTTApplication extends DaggerApplication implements Application.ActivityLifecycleCallbacks {

    private static JTTApplication instance = new JTTApplication();

    private SharedPreferences sharedPreferences = null;

    public static JTTApplication getInstance() {
        return instance;
    }


    //One more test commit
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        registerActivityLifecycleCallbacks(this);
        sharedPreferences = SharedPreferenceHelper.getInstance().getSharedPreferences(this);
//        realmCreator = new RealmCreatorImpl(mInstance, provideConfigurationIfAvailable(), sharedPreferences);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifeCycleObserver());
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        broadcastIntent();
    }

    public void broadcastIntent() {
        Intent intent = new Intent("com.bookatable.INTENT_LOCALE_CHANGED");
        sendBroadcast(intent);
    }

    /**
     * App lifecycle observer that exposes callback method denoting APP_IN_FOREGROUND Event
     * We initiate/resume DBSync
     */
    public static LocalRepository localRepository;

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    class AppLifeCycleObserver implements LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onStart() {
            Log.d("VJNDR7", "DB setup Start");

//            Connectivity connectivity = new Connectivity();
//            BatchSizeManager batchSizeManager = new BatchSizeManager(getApplicationContext(), connectivity);
//            Retrofit retrofit = ApiClient.getClient();
//            ApiInterface requestInterface = retrofit.create(ApiInterface.class);
//            localRepository = new LocalRepository(realmCreator);
//            CustomerChangesMapper restaurantChangesMapper = new CustomerChangesMapper();
//
//            CustomerChangesManager customerChangesManager = new CustomerChangesManager(batchSizeManager, requestInterface, localRepository, restaurantChangesMapper, sharedPreferences);
//
//            restaurantSyncRequest = new RestaurantSyncRequest(customerChangesManager);

//            deleteRealmDBOnceVersionUpdate();
//            boolean isSyncRequired = sharedPreferences.getBoolean(AppConstants.PreferenceTags.PREFS_OFFLINE_SYNC_REQUIRED, true);
//            if (isSyncRequired) {
                new DBSetupAsyncTask(getBaseContext(),
                        sharedPreferences, localRepository).execute();
//            } else {
//                    Log.d("VJNDR7", "VJNDR7 initRequest Started from VJApp");
////                    restaurantSyncRequest.initRequest();
//                }
//            }
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void onStop() {
            Log.d("VJNDR7", "DB setup Stop");
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void onDestroy() {
            Log.d("VJNDR7", "DB setup - On Destroy called!");
        }
    }

}
