package com.jjtraveltech.android.uimodules.articleList.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.jjtraveltech.android.JTTApplication;
import com.jjtraveltech.android.dagger.module.JTTDBModule;
import com.jjtraveltech.android.dataconverters.ArticleMapper;
import com.jjtraveltech.android.dataconverters.database.RealmCreator;
import com.jjtraveltech.android.dataconverters.database.RealmCreatorImpl;
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.ArticlesDAO;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;
import com.jjtraveltech.android.uimodules.articleList.extractDatabase.JTTRealmMigration;
import com.jjtraveltech.android.uimodules.articleList.view.ArticleListView;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalDataRepository;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository;
import com.jjtraveltech.android.util.helper.AppConstants;
import com.jjtraveltech.android.util.helper.Connectivity;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Response;
import rx.Subscriber;


public class BaseListPresenter implements ArticleListActions {

    private final LocalDataRepository localRepository;
    private SharedPreferences sharedPreferences;
    private ArticleListView view;

    private CompositeDisposable compositeDisposable;
    private ArticleMapper articleMapper;
    private Connectivity connectivity;

    public ArrayList<ArticlesResponse> articlesResponses;

    public boolean isFistCall = true;
//    RealmCreator realmCreator;

    @Inject
    public BaseListPresenter(ArticleListView view, SharedPreferences sharedPreferences, LocalRepository localRepository, ArticleMapper articleMapper, Connectivity connectivity) {
        this.view = view;
        this.sharedPreferences = sharedPreferences;
        this.localRepository = localRepository;
        this.articleMapper = articleMapper;
        this.connectivity = connectivity;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewCreated() {
        view.hideOfflineScreen();
        view.showSpinner();

        sharedPreferences.edit().putBoolean(AppConstants.PREFS_NEAR_ME_CLICKED, false).apply();
        boolean isInternetAvailable = connectivity.isMobileConnected(view.getContext());
        if (!isInternetAvailable) {
            sharedPreferences.edit().putBoolean(AppConstants.PREFS_NEAR_ME_CLICKED, true).apply();
            return;
        }

//        realmCreator = new RealmCreatorImpl(JTTApplication.getInstance(), provideConfigurationIfAvailable(), sharedPreferences);

        view.hideOfflineScreen();
        fetchArticles(view.getContext());
    }

    private void fetchRestaurantsNearMe(boolean showDistanceInCards) {
        /*localRepository.
                fetchRestaurantsNearMe().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                map(restaurants -> articleMapper.convertToRestaurantAdapter(restaurants, showDistanceInCards)).
                subscribe(new Observer<ArrayList<ArticleOverview>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        view.showSpinner();
                    }

                    @Override
                    public void onNext(ArrayList<ArticleOverview> restaurants) {

//                        if (articlesResponses != null && articlesResponses.size() > 0) {
//                            articlesResponses = articlesResponses;
//                            view.hideErrorView();
//                            view.showSpinner();
//                            showDataOnUI(articlesResponses);
//                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Error getting restaurant from database");
                        view.hideSpinner();
                    }

                    @Override
                    public void onComplete() {

                    }
                });*/
    }

    private void fetchArticles(Context context) {

        ArticlesDAO.Companion.getInstance(context)
                .getArticles()
                .subscribeOn(rx.schedulers.Schedulers.io())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ArrayList<ArticlesResponse>>>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Response<ArrayList<ArticlesResponse>> articlesResponse) {
                        if (articlesResponse != null && articlesResponse.body() != null
                                && articlesResponse.body().size() > 0) {

                            articlesResponses = articlesResponse.body();
                            mapDataToTable(articlesResponses, context);
                            showDataOnUI(articlesResponses);

                        }

                    }
                });
    }


    @Override
    public void onPlayServiceUpdated() {
        fetchRestaurantsNearMe(false);
    }

    /**
     * Remove any observable we were listening to
     */
    @Override
    public void onViewDetach() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

    /**
     * insert/update/delete data in database
     */
    private void mapDataToTable(ArrayList<ArticlesResponse> response, Context context) {
        try {
//            Log.d("DB_Log","Insertion started!!!");
//            Realm.init(context);
//            Realm realm = Realm.getInstance(provideConfigurationIfAvailable());
//            Realm realm = Realm.getDefaultInstance();

            ArrayList<class_result> databaseCompatible = new ArrayList<>();
            for (ArticlesResponse articlesResponses : response) {

                class_result classresult = new class_result();
                classresult.setId(articlesResponses.getId());
                classresult.setCreatedAt(articlesResponses.getCreatedAt());
                classresult.setComments(articlesResponses.getComments());
                classresult.setContent(articlesResponses.getContent());
                classresult.setLikes(articlesResponses.getLikes());
//            if (articlesResponses.getMedia() != null
//                    && articlesResponses.getMedia().size() > 0
//                    && articlesResponses.getMedia().get(0).getImage() != null)
//                classresult.setImageURL(articlesResponses.getMedia().get(0).getImage());

                databaseCompatible.add(classresult);
            }
            localRepository.addOrUpdateArticleList(databaseCompatible);
            Log.d("DB_Log","Insertion completed!!!");
//            realm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDataOnUI(ArrayList<ArticlesResponse> articleOverviews) {
        if (articleOverviews.size() > 0)
            if (isFistCall) {
                view.hideSpinner();
                view.showData(articleOverviews);
                isFistCall = false;
            } else {
                view.updateData(articleOverviews);
            }
    }

    /*public RealmConfiguration provideConfigurationIfAvailable() {
        Realm.init(JTTApplication.getInstance());
        String parentPath = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, "");
        boolean isExist = new File(parentPath).exists();
        if (!isExist) {
            return null;
        }
        String dbName = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_NAME, "");
        return new RealmConfiguration.Builder().schemaVersion(AppConstants.PreferenceTags.SCHEMA_VERSION)
                .directory(new File(parentPath))
                .name(dbName)
                .addModule(new JTTDBModule())
                .migration(new JTTRealmMigration())
                .build();
    }*/
}