package com.jjtraveltech.android.uimodules.articleList.extractDatabase;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class JTTRealmMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        Log.d("Redwood", "JTTRealmMigration 0000 oldVersion = " + oldVersion + " newVersion = " + newVersion);
        //isStarDeal
        if (newVersion == 2) {
            RealmObjectSchema classResultSchema = schema.get("class_result");
            Log.d("Redwood", "JTTRealmMigration 1111 offerSchema == " + classResultSchema.toString() + "Name == " + classResultSchema.getClassName());
            classResultSchema

                    .addField("image_file", Boolean.class, FieldAttribute.REQUIRED);
                    /*.addField("offerName",String.class,FieldAttribute.REQUIRED)*/
//                    .removeField("offerType");
            Log.d("Redwood", "JTTRealmMigration 2222");
        }
    }
}
