package com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.endpoints;

import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Inter provides ready methods to class various API's with specific methods
 */
public interface ChangesApiInterface {

    @GET("jet2/api/v1/blogs?page=2&limit=10")
    Observable<Response<ArrayList<ArticlesResponse>>> fetchChanges();
}
