package com.jjtraveltech.android.dagger.module;

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result;

import io.realm.annotations.RealmModule;

@RealmModule(classes = {class_result.class})
public class JTTDBModule {
}