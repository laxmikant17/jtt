package com.jjtraveltech.android.uimodules.mainScreen.interfaces;

import com.jjtraveltech.android.dataconverters.database.RealmCreator;
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

import io.realm.Realm;


public class LocalRepository implements LocalDataRepository {


    private RealmCreator realmCreator;

    @Inject
    public LocalRepository(RealmCreator realmCreator) {
        this.realmCreator = realmCreator;
    }

    @Override
    public void addOrUpdateArticleList(@NotNull ArrayList<class_result> articleResponseArrayList){//, RealmCreator realmCreator) {
        Realm realm = realmCreator.getRealm();
        realm.beginTransaction();
        realm.insertOrUpdate(articleResponseArrayList);
        realm.commitTransaction();
        realmCreator.close();
    }
}
