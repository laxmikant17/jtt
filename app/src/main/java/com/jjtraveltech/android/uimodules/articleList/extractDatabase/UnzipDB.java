package com.jjtraveltech.android.uimodules.articleList.extractDatabase;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;

import com.jjtraveltech.android.util.helper.AppConstants;
import com.jjtraveltech.android.util.helper.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;


public class UnzipDB extends Thread {

    private Context context;
    private SharedPreferences sharedPreferences;

    //TODO asking for environmentType
    public UnzipDB(android.content.Context context, SharedPreferences sharedPreferences) {
        this.context = context;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void run() {
        super.run();
        Timber.d("Current thread name %s", Thread.currentThread().getName());
        copyDataBaseToDir();
    }

    private void copyDataBaseToDir() {
        if (context == null)
            return;
        if (sharedPreferences == null)
            return;
        boolean isSyncRequired = sharedPreferences.getBoolean(AppConstants.PreferenceTags.PREFS_OFFLINE_SYNC_REQUIRED, true);
        boolean isParentDirectoryExist = checkIfParentDirectoryExist();
        Timber.d("Is sync required? => %s", isSyncRequired);
        Timber.d("Is parent directory exist? => %s", isParentDirectoryExist);

        if (!isSyncRequired && isParentDirectoryExist) {
            Timber.d("Sync is not required");
            setupRealmConfiguration();
            return;
        }
        Timber.d("Sync is required");
        String filename = Environment.INSTANCE.getFileNameByEnvironment();
        AssetManager assetManager = context.getAssets();
        try {
            InputStream in = assetManager.open(filename);
            //absolute path to the directory on the filesystem, filename
            File outFile = new File(context.getFilesDir(), filename);
            OutputStream out = new FileOutputStream(outFile);
            Timber.d("Internal file path is %s and file name is %s", outFile.getAbsolutePath(), filename);

            //final path should look like this
            //data/user/0/com.bookatable.android.debug/files/bat_database.zip

            try {
                Timber.d("Copying contents of assets in internal storage");
                //copy asset file contents into this file
                copyFileContentsToDestination(in, out);
            } catch (Exception e) {
                Timber.e(e, "Failed to copy contents from asset");
                return;
            }

            //Once zip file is created we need to extract the file
            boolean areContentExtracted = extractContents(context, outFile);
            if (!areContentExtracted) {
                Timber.tag(UnzipDB.class.getSimpleName()).e("Error while copying db file");
            }
            in.close();
            out.flush();
            out.close();
        } catch (Exception e) {
            Timber.e(e, "Failed to copy asset file: %s", filename);
        }
    }

    private boolean checkIfParentDirectoryExist() {
        String parentPath = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, "");
        File file = new File(parentPath);
        return file.exists();
    }


    /**
     * Extracts zipped file contents
     *
     * @param context         context instance
     * @param destinationFile zipped file instance
     * @return boolean indicating if file was extracted
     */
    private boolean extractContents(Context context, File destinationFile) {
        byte[] buffer = new byte[1024];
        File databaseFile = null;

        try {
            //data/user/0/com.bookatable.android.debug/files/bat_database.zip
            final String filePath = destinationFile.getAbsolutePath();
            //data/user/0/com.bookatable.android.debug/files/
            final String parentPath = destinationFile.getParentFile().getAbsolutePath();

            //get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(filePath));
            //get the zipped file list entry
            ZipEntry zipEntry = zis.getNextEntry();
            boolean fileFound = false;
            Timber.d("Iterating over zipped file");
            while (zipEntry != null && !fileFound) {
                String fileName = zipEntry.getName();
                Timber.d("Zipped file name is %s", fileName);
                if (fileName.endsWith("realm")) {
                    fileFound = true;
                }
                if (fileName.startsWith(".")) {
                    continue;
                }
                databaseFile = new File(parentPath + File.separator + fileName);
                Timber.d("Database file's absolute path would be : %s", databaseFile.getAbsoluteFile());
                //create parent dir if not created already
                boolean isParentDirCreated = new File(databaseFile.getParent()).mkdirs();
                if (!isParentDirCreated) {
                    boolean isParentFileExist = new File(databaseFile.getParent()).exists();
                    if (!isParentFileExist) {
                        Timber.e("Failed to create parent directory,canceling extraction");
                        return false;
                    }
                    Timber.e("Parent dir %s was not created as it was created before", parentPath);
                }
                Timber.d("Parent directory is available, now copying contents to database file");
                FileOutputStream fos = new FileOutputStream(databaseFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
            Timber.d("database file is created, link the file to RealmConfig");
            //Database file is created, we now link it to realm configuration
            setupRealmForFirstTime(context, databaseFile);
            return true;
        } catch (IOException ex) {
            Timber.e(ex, "Error while extracting contents =>159 ");
            return false;
        }
    }

    /**
     * copies content of input stream (db - asset file) to internal db file
     *
     * @param in  input stream instance
     * @param out output stream instance
     * @throws IOException ioException
     */
    private static void copyFileContentsToDestination(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    /**
     * Sets up realm database and sharedpreference
     * Change migration version if table is modified or new table is created
     */
    private void setupRealmForFirstTime(Context context, File filepath) {
        Timber.d("Database file is now being created");
        Realm.init(context);
        final RealmConfiguration configuration = new RealmConfiguration.Builder().
                directory(filepath.getParentFile()).
                name(filepath.getName()).
                schemaVersion(AppConstants.PreferenceTags.SCHEMA_VERSION).
                build();
        Realm.setDefaultConfiguration(configuration);
        sharedPreferences.edit().putBoolean(AppConstants.PreferenceTags.PREFS_OFFLINE_SYNC_REQUIRED, false).apply();
        sharedPreferences.edit().putString(AppConstants.PreferenceTags.PREFS_REALM_NAME, filepath.getName()).apply();
        sharedPreferences.edit().putString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, filepath.getParentFile().getAbsolutePath()).apply();
    }

    private void setupRealmConfiguration() {
        Timber.d("Database file was already created");
        //run migration if necessary
        Realm.init(context);
        RealmConfiguration realmConfiguration = configuringRealm();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private RealmConfiguration configuringRealm() throws IllegalArgumentException {
        String dbName = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_NAME, "");
        String parentPath = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, "");
        return new RealmConfiguration.Builder()
                .schemaVersion(AppConstants.PreferenceTags.SCHEMA_VERSION)
                .directory(new File(parentPath))
                .name(dbName)
                .migration(new JTTRealmMigration())
                .build();
    }
}
//  output file would look like /data/user/0/com.jjtraveltech.android.debug/files/default.realm
