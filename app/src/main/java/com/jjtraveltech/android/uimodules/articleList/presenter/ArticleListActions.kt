package com.jjtraveltech.android.uimodules.articleList.presenter

interface ArticleListActions {
    fun onViewCreated()

    fun onPlayServiceUpdated()

    fun onViewDetach()
}
